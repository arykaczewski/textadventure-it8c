﻿using System;

namespace Storyspiel
{
    public class ElternverfolgenSzenario : ISzenario
    {
        

        public bool Ergebnis => SzenarioDurchspielen();

        public bool SzenarioDurchspielen()
        {
            Console.WriteLine("Eltern verfolgen? (true / false eingeben)");
            return Program.BooleanEinlesen();
        }
    }
}