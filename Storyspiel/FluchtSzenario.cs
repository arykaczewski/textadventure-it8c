﻿using System;

namespace Storyspiel
{
    public class FluchtSzenario : ISzenario
    {
        public bool Ergebnis => SzenarioDurchspielen();
        public bool SzenarioDurchspielen()
        {
            Console.WriteLine("Aus dem Haus flüchten?");
            return Program.BooleanEinlesen();
        }
    }
}