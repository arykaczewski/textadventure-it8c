﻿using System;

namespace Storyspiel
{
    public class HuetteBetretenSzenario : ISzenario
    {
        public bool Ergebnis => SzenarioDurchspielen();

        public bool SzenarioDurchspielen()
        {
            Console.WriteLine("Ihr sucht einen Unterstand und findet eine Hütte auf einer Lichtung im Wald.");
            Console.WriteLine("Hütte betreten?");
            return Program.BooleanEinlesen();
        }
    }
}