using System;                    //benoetigte Repositories werden importiert
using System.Net.NetworkInformation;    //
using System.Text;    //
using System.Threading;    //

namespace Storyspiel
{
    public static class Program
    {
        private static void Main()
        {
            var elternVerfolgenSzenario = new ElternverfolgenSzenario(); //Objekt wird erzeugt
            var huetteBetretenSzenario = new HuetteBetretenSzenario(); //Objekt wird erzeugt
            var fluechtenSzenario = new FluchtSzenario(); //Objekt wird erzeugt
            var befreieSzenario = new BefreiSzenario(); //Objekt wird erzeugt

            
            var sb = new StringBuilder(); //Objekt wird erzeugt

            sb.Append("Du und dein Bruder wurden von deinen Eltern im Wald ausgesetzt.");

            Console.WriteLine(sb.ToString());
            if (elternVerfolgenSzenario.Ergebnis)
            {
                Console.WriteLine("Eure Eltern drehen um und überfahren euch.\n ENDE");
                
                Thread.Sleep(10000);
            }
            else
            {
                if (huetteBetretenSzenario.Ergebnis)
                {
                    Console.WriteLine(
                        "Ihr betretet die vermoderte Hütte. - Eine alte Frau mit einer großen Warze auf der Rabennase öffnet euch die Tür.");
                    Console.WriteLine(
                        "Sie bietet euch Essen und Trinken an und lässt euch für einige Zeit in einem kleinen Zimmer einer Ecke der Hütte leben. Die erste Zeit kommt ihr mit der Frau, die dem Aussehen einer Hexe ähnelt, gut zurecht. Jedoch wird sie nach einiger Zeit merkwürdig und geheimnisvoll. Auf Nachfrage, warum sie im Wald lebt, gibt sie euch keine richtige Antwort und weicht anderen Fragen auch eher aus als euch das Fragezeichen aus dem Kopf zu nehmen.");

                    if (fluechtenSzenario.Ergebnis)
                    {
                        Console.WriteLine("Ihr trefft auf einen Bären der euch verputzt. \n ENDE");
                        
                        Thread.Sleep(10000);
                    }

                    else
                    {
                        Console.WriteLine(
                            "Ihr bleibt im Haus der Hexe und hofft auf Normalisierung der Situation. - Eines Morgens wachst du in einem Schuppen, halb so groß wie das Zimmer, auf. Deine Schwester muss die Hexe im Haushalt unterstützen, Nahrung im Wald sammeln und bei der Instandhaltung der kleinen Hütte helfen.");
                        Console.WriteLine(
                            "So geht es Tag für Tag und Woche für Woche. Es geht so lang, dass ihr nicht mehr wisst, welcher Wochentag oder Monat ist.\nDie Hexe missbraucht die Kräfte deiner Schwester und lässt sie von morgens bis abends arbeiten und schuften. Du hingegen wirst gemästet, wie ein Hausschwein.\nEines Tages findest du einen kleinen, spitzen Gegenstand. Du weißt nicht, ob es ein Stock oder ein Knochen vom letzten Eichhörnchen ist, dass deine Schwester für dich gekocht hat.");

                        if (befreieSzenario.Ergebnis)
                        {
                            Console.WriteLine(
                                "Du versuchst sofort, sobald die Hexe dich aus den Augen lässt, damit das Schloss zu knacken. Die Hexe erwischt dich unglücklicher Weise und kocht euch. \n ENDE");
                            
                            Thread.Sleep(10000);
                        }

                        else
                        {
                            Console.WriteLine(
                                "Du versteckst den Gegenstand und wartest auf einen besseren Augenblick, um das Schloss zu knacken oder die Hexe zu bedrohen. In der Nacht befreist du dich aus dem vermoderten, aber immer noch stabilen Hüttchen. Dabei zerbricht der Knochen und eignet sich nicht mehr als Waffe. Du weckst deine Schwester. Die Hexe hat davon bisher nichts mitbekommen und liegt immer noch in ihrem kleinen Bett hinter dem Ofen.\nIhr beide schleicht zum Bett der Hexe, nehmt sie und schmeißt sie in den heißen Ofen.\nNach einem lauten Schrei herrscht Stille im Haus...");
                        }
                    }
                }
                else
                {
                    Console.WriteLine(
                        "Ihr beachtet die Hütte nicht weiter, geht in den tiefen Wald wieder hinein und findet nach einiger Zeit eine umgefallene alte Eiche.");
                    Console.WriteLine(
                        "Nach einigen Tagen, die ihr dort Schutz gesucht habt, kommt eine alte Frau vorbei und bietet euch Unterstützung an. Sie bringt euch in ihr altes, herab gekommenes Haus. Es kommt euch bekannt vor…");
                    Console.WriteLine(
                        "Sie bietet euch Essen und Trinken an und lässt euch für einige Zeit in einem kleinen Zimmer einer Ecke der Hütte leben. Die erste Zeit kommt ihr mit der Frau, die dem Aussehen einer Hexe ähnelt, gut zurecht. Jedoch wird sie nach einiger Zeit merkwürdig und geheimnisvoll. Auf Nachfrage, warum sie im Wald lebt, gibt sie euch keine richtige Antwort und weicht anderen Fragen auch eher aus als euch das Fragezeichen aus dem Kopf zu nehmen.");

                    if (fluechtenSzenario.Ergebnis)
                    {
                        Console.WriteLine("Ihr trefft auf einen Bären der euch verputzt.");
                    }

                    else
                    {
                        Console.WriteLine(
                            "Ihr bleibt im Haus der Hexe und hofft auf Normalisierung der Situation. - Eines Morgens wachst du in einem Schuppen, halb so groß wie das Zimmer, auf. Deine Schwester muss die Hexe im Haushalt unterstützen, Nahrung im Wald sammeln und bei der Instandhaltung der kleinen Hütte helfen.");
                        Console.WriteLine(
                            "So geht es Tag für Tag und Woche für Woche. Es geht so lang, dass ihr nicht mehr wisst, welcher Wochentag oder Monat ist.\nDie Hexe missbraucht die Kräfte deiner Schwester und lässt sie von morgens bis abends arbeiten und schuften. Du hingegen wirst gemästet, wie ein Hausschwein.\nEines Tages findest du einen kleinen, spitzen Gegenstand. Du weißt nicht, ob es ein Stock oder ein Knochen vom letzten Eichhörnchen ist, dass deine Schwester für dich gekocht hat.");

                        if (befreieSzenario.Ergebnis)
                        {
                            Console.WriteLine(
                                "Du versuchst sofort, sobald die Hexe dich aus den Augen lässt, damit das Schloss zu knacken. Die Hexe erwischt dich unglücklicher Weise und kocht dich.\nEnde");
                            Thread.Sleep(10000);
                        }

                        else
                        {
                            Console.WriteLine(
                                "Du versteckst den Gegenstand und wartest auf einen besseren Augenblick, um das Schloss zu knacken oder die Hexe zu bedrohen. In der Nacht befreist du dich aus dem vermoderten, aber immer noch stabilen Hüttchen. Dabei zerbricht der Knochen und eignet sich nicht mehr als Waffe. Du weckst deine Schwester. Die Hexe hat davon bisher nichts mitbekommen und liegt immer noch in ihrem kleinen Bett hinter dem Ofen.\nIhr beide schleicht zum Bett der Hexe, nehmt sie und schmeißt sie in den heißen Ofen.\nNach einem lauten Schrei herrscht Stille im Haus...\n ENDE");
                            Thread.Sleep(10000);
                        }
                    }
                }
            }
        }

        public static bool BooleanEinlesen()
        {
            var eingabe = Console.ReadLine();
            bool antwort;

            while (!bool.TryParse(eingabe, out antwort))
            {
                Console.WriteLine("Dir stehen nur true oder false zur Verfügung.");
                eingabe = Console.ReadLine();
            }

            return antwort;
        }
    }
}