﻿namespace Storyspiel
{
    public interface ISzenario //Regelset für jedes Sezenario
    {
        bool SzenarioDurchspielen();
    }
}