﻿using System;

namespace Storyspiel
{
    public class BefreiSzenario : ISzenario
    {
        public bool Ergebnis => SzenarioDurchspielen();

        
        
        public bool SzenarioDurchspielen()
        {
            Console.WriteLine("Jetzt befreien statt auf den Abend zu warten?");
            return Program.BooleanEinlesen();
        }
    }
}